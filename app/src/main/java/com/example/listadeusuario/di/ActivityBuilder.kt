package com.example.listadeusuario.di

import com.example.listadeusuario.ui.detalhes.DetalhesActivity
import com.example.listadeusuario.ui.detalhes.DetalhesActivityProviders
import com.example.listadeusuario.ui.home.HomeActivity
import com.example.listadeusuario.ui.home.HomeActivityProviders
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [HomeActivityProviders::class])
    abstract fun bindMainActivity(): HomeActivity

    //Adding subclasses
    @ContributesAndroidInjector(modules = [DetalhesActivityProviders::class])
    abstract fun bindDetalhesActivity(): DetalhesActivity
}