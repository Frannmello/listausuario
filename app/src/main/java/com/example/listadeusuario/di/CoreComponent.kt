package com.example.listadeusuario.di

import android.app.Application
import com.example.listadeusuario.core.App
import com.example.listadeusuario.ui.detalhes.DetalhesPresenterModule
import com.example.listadeusuario.ui.home.HomePresenterModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, NetworkModule::class,  ActivityBuilder::class,
    HomePresenterModule::class, DetalhesPresenterModule::class])
interface CoreComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): CoreComponent
    }


}