package com.example.listadeusuario.ui.detalhes

import com.example.listadeusuario.data.source.repository.AppRepositoryDetalhe
import dagger.Module
import dagger.Provides

@Module
class DetalhesPresenterModule {
    @Provides
    fun DetalhesPresenterModule(appRepositoryDetalhe: AppRepositoryDetalhe): DetalhesContrato.Presenter =
        DetalhesPresenter(appRepositoryDetalhe)
}