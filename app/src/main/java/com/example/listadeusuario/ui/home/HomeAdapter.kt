package com.example.listadeusuario.ui.home

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.listadeusuario.R
import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.ui.detalhes.DetalhesActivity
import kotlinx.android.synthetic.main.user_item.view.*


class HomeAdapter(
    private var items: MutableList<users> = arrayListOf()
) : RecyclerView.Adapter<HomeAdapter.SimpleHolder>() {
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SimpleHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleHolder =
        SimpleHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false))

    inner class SimpleHolder(
        val view: View

    ) : RecyclerView.ViewHolder(view) {

        fun bind(user: users) = with(user) {
            itemView.name.text = user.login
            com.squareup.picasso.Picasso.with(itemView.context).load(user.avatarUrl).into(itemView.profile_pic)

            itemView.setOnClickListener {
                irTelaDetalhes(itemView.context,user.id)
            }
        }
    }

    fun add(list: MutableList<users>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun irTelaDetalhes(context : Context, userId : Int) {
        val intent = Intent(context, DetalhesActivity::class.java)
        intent.putExtra("userId", userId)
        context.startActivity(intent)
    }

}