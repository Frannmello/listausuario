package com.example.listadeusuario.ui.detalhes

import android.os.Bundle
import com.example.listadeusuario.R
import com.example.listadeusuario.util.replaceFragmentInActivity
import dagger.android.support.DaggerAppCompatActivity

class DetalhesActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes)
        var data = intent.extras
        val userId = data!!.getInt("userId")

        val detalheFragment = supportFragmentManager.findFragmentById(
            R.id.homeContentFrame
        ) as DetalhesFragment? ?: DetalhesFragment
            .newInstance(userId).also {
                replaceFragmentInActivity(it, R.id.detalheContentFrame)
            }

    }

}
