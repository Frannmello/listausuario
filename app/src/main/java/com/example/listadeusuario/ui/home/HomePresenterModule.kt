package com.example.listadeusuario.ui.home

import com.example.listadeusuario.data.source.repository.AppRepositoryUser
import dagger.Module
import dagger.Provides

@Module
class HomePresenterModule {
    @Provides
    fun providesHomePresenter(appRepositoryUser: AppRepositoryUser): HomeContrato.Presenter =
        HomePresenter(appRepositoryUser)
}