package com.example.listadeusuario.ui.home

import android.util.Log
import com.example.listadeusuario.data.source.repository.AppRepositoryUser
import io.reactivex.disposables.CompositeDisposable

class HomePresenter (private val appRepositoryUser: AppRepositoryUser): HomeContrato.Presenter{
    private val compositeDisposable = CompositeDisposable()
    var view: HomeContrato.View? = null
    private val TAG: String = HomePresenter::class.java.simpleName

    override fun getUsers() {
        appRepositoryUser.getUsers(
            {
                Log.d(TAG, "getFoods.success() called with: $it")
                view?.removeWait()
                view?.showUser(it)
                if (it!=null){
                    //ver o que fazer aqui
                   // insertToDb(it.results)
                    Log.d(TAG, "deu certoooo $it")
                }
            },
            {
                Log.d(TAG, "getFoods.error() called with: $it")
                view?.removeWait()
                view?.onFailure(it)
            },
            {
                view?.removeWait()
            }
        ).also {
            if (it !=null)
                compositeDisposable.add(it)
        }
    }

    override fun detachView(view: HomeContrato.View) {
        this.view = null
    }

    override fun attachView(view: HomeContrato.View) {
        this.view = view
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}