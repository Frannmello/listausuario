package com.example.listadeusuario.ui.home

import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.data.source.remote.ApiError
import mvp.cooking.com.foodlist.view.base.BasePresenter
import mvp.cooking.com.foodlist.view.base.BaseView

interface HomeContrato {

    interface View : BaseView {
        fun showWait()
        fun removeWait()
        fun showUser(user: List<users>)
        fun onFailure(apiError: ApiError)
    }

    interface Presenter : BasePresenter<View> {
        fun getUsers()
    }
}