package com.example.listadeusuario.ui.detalhes

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DetalhesActivityProviders {
    @ContributesAndroidInjector
    abstract fun provideDetalhesFragment(): DetalhesFragment
}