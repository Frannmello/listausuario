package com.example.listadeusuario.ui.detalhes

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.listadeusuario.R
import com.example.listadeusuario.data.model.detalhes
import com.example.listadeusuario.data.source.remote.ApiError
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home1.*
import javax.inject.Inject


class DetalhesFragment : DaggerFragment(), DetalhesContrato.View {

    private val TAG: String = DetalhesFragment::class.java.simpleName
    var detalheList : List<detalhes> = arrayListOf()
    @Inject
    lateinit var presenter: DetalhesContrato.Presenter
    private lateinit var root: View
    private  var userId : Int =0
    val adapter : DetalhesAdapter by lazy { DetalhesAdapter(arrayListOf()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.fragment_detalhes, container, false)
        if (arguments != null) {
            userId = arguments!!.getInt("userId")
            Log.e("valor userId : ","" +userId)
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        presenter.getUsersDetalhes(userId)
    }

    override fun showWait() {
        Log.e(TAG,"showWait")
        progressBar_home.visibility=View.VISIBLE
    }

    override fun removeWait() {
        Log.e(TAG,"removeWait")
        progressBar_home.visibility=View.GONE
    }

    override fun showUserDetalhes(detalhe: detalhes) {
        rv_main_home.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_main_home.adapter = adapter

        Toast.makeText(context, " detalhes " + detalhe.name, android.widget.Toast.LENGTH_LONG).show()
        detalheList =  listOf(detalhe)
        if (!detalheList.isEmpty()) {
            adapter.clear()
            adapter.add(detalheList.toMutableList())
        }else{
            Toast.makeText(context, context?.getString(R.string.empty_list), android.widget.Toast.LENGTH_LONG).show()
        }
    }

    override fun onFailure(apiError: ApiError) {
        Log.e(TAG,"onFailure")
        progressBar_home.visibility=View.GONE
        Toast.makeText(context,apiError.getErrorMessage(),Toast.LENGTH_LONG).show()
    }


    companion object {
        @JvmStatic
        val FRAGMENT_NAME: String = DetalhesFragment::class.java.name
        fun newInstance(userId:Int): DetalhesFragment {
            val fragment = DetalhesFragment()
            val args = Bundle()
            args.putInt("userId", userId)
            fragment.arguments = args
            return fragment
        }

    }
}
