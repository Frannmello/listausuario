package com.example.listadeusuario.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.listadeusuario.R
import com.example.listadeusuario.data.model.userList
import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.data.source.remote.ApiError
import com.example.listadeusuario.ui.detalhes.DetalhesFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home1.*
import javax.inject.Inject


class HomeFragment : DaggerFragment(), HomeContrato.View {

    private val TAG: String = HomeFragment::class.java.simpleName


    @Inject
    lateinit var presenter: HomeContrato.Presenter
    val adapter : HomeAdapter by lazy { HomeAdapter(arrayListOf()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        presenter.getUsers()
    }

    override fun showWait() {
        Log.e(TAG,"showWait")
        progressBar_home.visibility=View.VISIBLE
    }

    override fun removeWait() {
        Log.e(TAG,"removeWait")
        progressBar_home.visibility=View.GONE
    }

    override fun showUser(user: List<users>) {
        Log.e(TAG,"showFoods")
        rv_main_home.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_main_home.adapter = adapter
        if (!user.isEmpty()) {
            adapter.clear()
            adapter.add(user.toMutableList())
        }else{
            Toast.makeText(context, context?.getString(R.string.empty_list), android.widget.Toast.LENGTH_LONG).show()
        }
    }

    override fun onFailure(apiError: ApiError) {
        Log.e(TAG,"onFailure")
        progressBar_home.visibility=View.GONE
        Toast.makeText(context,apiError.getErrorMessage(),Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView(this)
        presenter.destroy()
    }

    companion object {
        @JvmStatic
        val FRAGMENT_NAME: String = HomeFragment::class.java.name
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            return fragment
        }


    }
}
