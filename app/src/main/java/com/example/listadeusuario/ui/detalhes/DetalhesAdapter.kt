package com.example.listadeusuario.ui.detalhes

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.listadeusuario.R
import com.example.listadeusuario.data.model.detalhes
import kotlinx.android.synthetic.main.item_detalhe.view.*
import kotlinx.android.synthetic.main.user_item.view.*

class DetalhesAdapter(
    private var items: MutableList<detalhes> = arrayListOf()
) : RecyclerView.Adapter<DetalhesAdapter.SimpleHolder>() {
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SimpleHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleHolder =
        SimpleHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_detalhe, parent, false))

    inner class SimpleHolder(
        val view: View

    ) : RecyclerView.ViewHolder(view) {

        fun bind(detalhe: detalhes) = with(detalhe) {
            itemView.tv_nome.text = detalhe.name
            itemView.tv_email.text = detalhe.email as CharSequence?
            itemView.tv_localidade.text = detalhe.location
            itemView.tv_blog.text = detalhe.blog
            itemView.tv_companhia.text = detalhe.company as CharSequence?
            com.squareup.picasso.Picasso.with(itemView.context).load(detalhe.avatarUrl).into(itemView.iv_img)
        }
    }

    fun add(list: MutableList<detalhes>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }


}