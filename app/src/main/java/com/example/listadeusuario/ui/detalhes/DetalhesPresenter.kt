package com.example.listadeusuario.ui.detalhes

import android.util.Log
import com.example.listadeusuario.data.source.repository.AppRepositoryDetalhe
import io.reactivex.disposables.CompositeDisposable

class DetalhesPresenter(private val appRepositoryDetalhe: AppRepositoryDetalhe) : DetalhesContrato.Presenter {
    private val compositeDisposable = CompositeDisposable()
    var view: DetalhesContrato.View? = null
    private val TAG: String = DetalhesPresenter::class.java.simpleName

    override fun getUsersDetalhes(id: Int) {
        appRepositoryDetalhe.getUsersDetalhes(
            {
                view?.removeWait()
                view?.showUserDetalhes(it)
                if (it != null) {
                    //ver o que fazer aqui
                    // insertToDb(it.results)
                    Log.d(TAG, "deu certo detalhes :  $it")
                }
            },
            {
                Log.d(TAG, "getFoods.error() called with: $it")
                view?.removeWait()
                view?.onFailure(it)
            },
            {
                view?.removeWait()
            }
        ).also {
            if (it != null)
                compositeDisposable.add(it)
        }
    }

    override fun detachView(view: DetalhesContrato.View) {
        this.view = null
    }

    override fun attachView(view: DetalhesContrato.View) {
        this.view = view
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}