package com.example.listadeusuario.ui.home

import android.os.Bundle
import com.example.listadeusuario.R
import com.example.listadeusuario.util.replaceFragmentInActivity
import dagger.android.support.DaggerAppCompatActivity

class HomeActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val homeFragment = supportFragmentManager.findFragmentById(
            R.id.homeContentFrame
        ) as HomeFragment? ?: HomeFragment
            .newInstance().also {
                replaceFragmentInActivity(it, R.id.homeContentFrame)
            }
    }


}
