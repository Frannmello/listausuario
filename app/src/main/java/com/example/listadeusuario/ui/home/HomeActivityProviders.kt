package com.example.listadeusuario.ui.home

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeActivityProviders {
    @ContributesAndroidInjector
    abstract fun provideHomeFragment(): HomeFragment
}