package com.example.listadeusuario.ui.detalhes

import com.example.listadeusuario.data.model.detalhes
import com.example.listadeusuario.data.source.remote.ApiError
import mvp.cooking.com.foodlist.view.base.BasePresenter
import mvp.cooking.com.foodlist.view.base.BaseView

interface DetalhesContrato {

    interface View : BaseView {
        fun showWait()
        fun removeWait()
        fun showUserDetalhes(detalhe: detalhes)
        fun onFailure(apiError: ApiError)
    }

    interface Presenter : BasePresenter<View> {
        fun getUsersDetalhes(id : Int)
    }
}