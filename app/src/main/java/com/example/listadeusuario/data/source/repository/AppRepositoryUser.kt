package com.example.listadeusuario.data.source.repository

import com.example.listadeusuario.data.model.userList
import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.data.source.remote.ApiError
import io.reactivex.disposables.Disposable

interface AppRepositoryUser {
    fun getUsers(
        success: (List<users>) -> Unit,
        failure: (ApiError) -> Unit = {},
        terminate: () -> Unit = {}
    ): Disposable

    fun insertUser(user: users): Disposable
}