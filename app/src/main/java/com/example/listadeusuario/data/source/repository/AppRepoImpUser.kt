package com.example.listadeusuario.data.source.repository

import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.data.source.remote.ApiDisposable
import com.example.listadeusuario.data.source.remote.ApiError
import com.example.listadeusuario.data.source.remote.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AppRepoImpUser
    (val apiService: ApiService): AppRepositoryUser {
    private val TAG = AppRepoImpUser::class.java.simpleName
    override fun getUsers(success: (List<users>) -> Unit, failure: (ApiError) -> Unit, terminate: () -> Unit): Disposable {
        return apiService
            .getHome()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminate)
            .subscribeWith(
                ApiDisposable<List<users>>(
                    {

                        success(it)
                    },
                    failure
                )
            )
    }

    override fun insertUser(user: users): Disposable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}