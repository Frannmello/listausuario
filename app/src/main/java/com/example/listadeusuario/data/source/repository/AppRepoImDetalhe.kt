package com.example.listadeusuario.data.source.repository

import android.util.Log
import com.example.listadeusuario.data.model.detalhes
import com.example.listadeusuario.data.model.users
import com.example.listadeusuario.data.source.remote.ApiDisposable
import com.example.listadeusuario.data.source.remote.ApiError
import com.example.listadeusuario.data.source.remote.ApiService
import com.example.listadeusuario.ui.detalhes.DetalhesContrato
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AppRepoImDetalhe (val apiService: ApiService): AppRepositoryDetalhe{
    override fun getUsersDetalhes(
        success: (detalhes) -> Unit,
        failure: (ApiError) -> Unit,
        terminate: () -> Unit
    ): Disposable {
        return apiService
            .getDetalhes(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminate)
            .subscribeWith(
                ApiDisposable<detalhes>(
                    {
                       Log.e("deu certo ", "detalhe")
                        success(it)
                    },
                    failure
                )
            )
    }

    override fun insertDetalhe(detalhe: detalhes): Disposable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}