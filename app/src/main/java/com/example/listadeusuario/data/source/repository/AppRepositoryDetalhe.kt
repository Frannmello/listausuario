package com.example.listadeusuario.data.source.repository

import com.example.listadeusuario.data.model.detalhes
import com.example.listadeusuario.data.source.remote.ApiError
import io.reactivex.disposables.Disposable

interface AppRepositoryDetalhe {

    fun getUsersDetalhes(
        success: (detalhes) -> Unit,
        failure: (ApiError) -> Unit = {},
        terminate: () -> Unit = {}
    ): Disposable

    fun insertDetalhe(detalhe: detalhes): Disposable

}