package com.example.listadeusuario.data.source.remote

import com.example.listadeusuario.data.model.detalhes
import com.example.listadeusuario.data.model.userList
import com.example.listadeusuario.data.model.users
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("/users")
    fun getHome(
    ): Observable<List<users>>

    @GET("/users/{users}")
    fun getDetalhes(@Path("users")filtro: Int): Observable<detalhes>

}