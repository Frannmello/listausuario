package com.example.listadeusuario.core

import android.support.v4.app.FragmentManager
import com.example.listadeusuario.ui.detalhes.DetalhesFragment
import com.example.listadeusuario.ui.home.HomeFragment

object FragmentFactory{

    fun getHomeFragment(supportFragmentManager: FragmentManager): HomeFragment {
        var fragment = supportFragmentManager.findFragmentByTag(HomeFragment.FRAGMENT_NAME)
        if (fragment == null) {
            fragment = HomeFragment()
        }
        return fragment as HomeFragment
    }

    fun getDetalhesFragment(supportFragmentManager: FragmentManager): DetalhesFragment {
        var fragment = supportFragmentManager.findFragmentByTag(DetalhesFragment.FRAGMENT_NAME)
        if (fragment == null) {
            fragment = DetalhesFragment()
        }
        return fragment as DetalhesFragment
    }

}