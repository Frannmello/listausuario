package com.example.listadeusuario

import com.example.listadeusuario.data.source.repository.AppRepositoryUser
import com.example.listadeusuario.ui.home.HomeContrato
import com.example.listadeusuario.ui.home.HomePresenter
import com.nhaarman.mockito_kotlin.mock
import org.junit.After
import org.junit.Before
import org.junit.Test

class HomePresenterTest {
    lateinit var presenter: HomePresenter
    lateinit var view: HomeContrato.View
    lateinit var appRepository: AppRepositoryUser

    @Before
    fun setup() {
        view = mock()
        appRepository = mock()
        presenter = HomePresenter(appRepository)
        presenter.attachView(view)
    }

    @After
    fun dispose() {
        presenter.detachView(view)
    }


    @Test
    fun `should alert unknown error to view when fetching foods`() {

    }

    @Test
    fun `should show food list when fetching home`() {

    }
}